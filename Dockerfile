FROM ocaml/opam2 as builder

WORKDIR /tempDir
COPY . /tempDir
RUN sudo chown -R opam: /tempDir

RUN DEBIAN_FRONTEND=noninteractive sudo apt-get update && sudo apt-get install -y -q m4
RUN opam pin add ott https://gitlab.com/nomadic-labs/ott.git#json
RUN opam install ott

RUN eval $(opam env); make check-commited

RUN DEBIAN_FRONTEND=noninteractive sudo apt-get update && sudo apt-get install -y -q make python3 python3-pip
RUN pip3 install  --user --upgrade pip && python3 -m pip install --user -r requirements.txt

RUN opam install -y hevea

RUN git clone https://gitlab.com/tezos/tezos.git
ENV TEZOS_HOME tezos/
# hack
RUN eval $(opam env); make docs/index.html

FROM nginx:alpine
COPY --from=builder /tempDir/docs/ /usr/share/nginx/html/
