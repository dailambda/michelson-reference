* Building locally

Install dependencies using pip:

#+BEGIN_SRC bash
pip3 install -r requirements.txt
#+END_SRC

=hevea= is also needed to generate fancy semantics:

#+BEGIN_SRC bash
apt install hevea
#+END_SRC

or

#+BEGIN_SRC bash
opam install hevea
#+END_SRC

Then run

#+BEGIN_SRC bash
TEZOS_HOME=~/dev/tezos make docs/index.html
#+END_SRC

where =~/dev/tezos= is modified as necessary to point to a local
installation of tezos (necessary to find example contract).

Then run e.g. =xdg-open docs/index.html= to see the generated documentation.


** Rebuilding =michelson.json=

The reference depends on the metadata in =michelson.json= which is
generated from =michelson.ott=. Both are distributed in this
repository. If necessary to regenerate the =.ott= file, a patched
version of ott is required.

#+BEGIN_SRC bash
opam pin ott git@gitlab.com:nomadic-labs/ott.git#json
#+END_SRC

and then

#+BEGIN_SRC bash
make michelson.json
#+END_SRC

* Using docker

** Testing using Docker

#+BEGIN_SRC bash
docker build -t nomadiclabs/michelson-reference:test .
docker run -d -p 80:80 nomadiclabs/michelson-reference:test
#+END_SRC

And then open a browser and point it to =http://localhost/=,
e.g. =xdg-open http://localhost/=.

** In the Wild

#+BEGIN_SRC bash
docker build -t nomadiclabs/michelson-reference .
docker push nomadiclabs/michelson-reference
#+END_SRC


* Version for integration in documentation (not maintained)

To generate a version intended for integration in existing tezos
developer documentation:

#+BEGIN_SRC bash
TEZOS_HOME=~/dev/tezos make docs/michelson_reference.html
#+END_SRC


* Format of =michelson-meta.yaml=


The format of =michelson-meta.yaml= is verified against the schema
described in =michelson-meta-schema.yaml=. All fields are required
unless marked (optional)

 #+BEGIN_SRC yaml
   # A dictionary of categories
   categories:
     # format [category]: [category_description]
     # e.g.
     core: Set of core operations

   # A dictionary where each entry is the meta data for one michelson
   # instruction. The key must be the constructor of that instruction as
   # defined in michelson.ott. Typically, this is simply the name of the
   # instruction.
   instructions:
     # instruction opcode:
     ADD:
       # the category of the instruction. a string that must be the key of one of the
       # string: categories defined above, e.g. core / domain / etc.
       category: core
       # string: a short documentation, one phrase. can use rst markup.
       documentation_short: Adds two numerical values.
       # (optional), string: longer documentation, focus on intuitions. can use rst markup.
       documentation: "Adds numerical values. This instruction is polymorphic and accepts..."
       # (optional), string: if omitted, will be synthesized as described below.
       stack_effect:  'int : int : [] — int : []'
       # (optional) a list of examples, each of which is a dictionary with the following properties
       examples:
         - # (optional), string: name of the example
           name: Sum of integers
           # string: description of the example. can use rst markup.
           description: Sums up the integers.
           # string: path to the example, relative to src/bin_client/test/contracts/
           path: opcodes/add.tz
           # (optional), string: comma separated ranges of lines to be printed. 
           ranges: -2,4-6,8,10-
           # by supplying an example input / initial_storage and
           # final_storage, the coherency of the example can be verified by
           # running ``make examples_verify``. i.e, if the example is run
           # with ``initial_storage`` and ``input`` then the final storage
           # is ``final_storage``
           # string: the example parameter
           input: '(pair 2 2)'
           # string: the example initial storage
           initial_storage: 'None'
           # string: the example final storage
           final_storage: 'Some 4'
           # (optional) boolean. if True, then the information about input/initial_storage/final_storage
           # is not visible in the generated doc. this is useful for examples whose input/output is not interesting.
           hide_final_storage: False
           # (optional) string: text preceding the automatic description of the final storage
           final_storage_pre: "This text will appear before the final storage description."
           # (optional) string: text following the automatic description of the final storage
           final_storage_post: "This text will appear after the final storage description."

   # A dictionary where each entry is a type
   types:
     # ...
     chain_id:
       # A short one-line documentation, appears in titles and tables
       documentation_short: A chain identifier

       # Specifies the characteristics of the type, as displayed in the
       # comparative table in the beginning of the reference. Refer to
       # the reference itself for the definition of these properties.
       passable: true
       storable: true
       big_map_value: true
       pushable: true
       packable: true
       comparable: false

       # A longer documentation.
       documentation: |
         An identifier for a chain, used to distinguish the test and the main chains.
       # Examples of literal values of this type.
       examples:
         - '0x7a06a770'
#+END_SRC
