import argparse

from language_def import LanguageDefinition
import subprocess

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

rules_dir = 'templates/rules/'

def rule_name_to_latex(rn):
    replacements = {
        "_": "XX",
        "0": "Zero",
        "1": "One",
        "2": "Two",
        "3": "Three",
        "4": "Four",
        "5": "Five",
        "6": "Six",
        "7": "Seven",
        "8": "Eight",
        "9": "Nine",
    }
    for k, v in replacements.items():
        rn = rn.replace(k ,v)
    return rn

def generate_rules(lang_def):
    # print(os.environ['PATH'])

    for instr in lang_def.get_instructions():
        rules = instr['ty'].copy()
        rules.extend(instr['semantics'])

        for rule in rules:
            f = open(rules_dir + rule['name'] + ".tex", "w+")

            f.write(
                """\\documentclass[11pt]{article}
\\input{../../michelson_embed}
\\begin{document}
\\[\\ottdrule""" + rule_name_to_latex(rule['name']) + """{}\\]
\\end{document}
 """)
            f.close()

            subprocess.run(["hevea", rule['name'] + ".tex"], cwd=rules_dir)

            # run hevea
            f = open(rules_dir + rule['name'] + ".html", "r")
            html = f.read()

            # a beautiful hack to remove rule name prefixes
            html = html.replace('>bs_', '>')
            html = html.replace('>t_instr_', '>')

            parsed_html = BeautifulSoup(html, features="html.parser")
            f_standalone= open(rules_dir + rule['name'] + ".standalone.html", "w+")
            f_standalone.write(str(parsed_html.find('table')))
            f.close()


def main():
    parser = argparse.ArgumentParser(description='Pretty-print Michelson semantics rules from ott.')

    args = parser.parse_args()

    lang_def = LanguageDefinition(example_path=None)

    generate_rules(lang_def)

    print(f'Pretty printed semantics rules into {rules_dir}')

if __name__ == '__main__':
    main()
